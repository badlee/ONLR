/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var my = {
	app : null,
    connect : function(){
        my.navigator.pushPage("pages/home.html");                    
    },
    login : function(login,pwd){
        pwd = md5(pwd);
        var dialog = document.getElementById('dialog-loading');
        var done = function(){
            localStorage.login = login;
            location.pwd = pwd;
            my.connect();
            this.hide();
        };
        var fn = function(dialog){
            dialog.querySelector("span[text]").textContent = 'Identification...';
            dialog.show();
            setTimeout(done.bind(dialog),5000);
        }
        if (dialog) {
            fn(dialog);
        } else {
            ons.createDialog('dialogs/loading.html')
            .then(function(dialog) {
                fn(dialog);
            });
        }
    },
    register : function(login,pwd){
        pwd = md5(pwd);
        var dialog = document.getElementById('dialog-loading');
        var done = function(){
            localStorage.login = login;
            location.pwd = pwd;
            my.connect();
            this.hide();
        };
        var fn = function(dialog){
            dialog.querySelector("span[text]").textContent = 'Enregistrement...';
            dialog.show();
            setTimeout(done.bind(dialog),5000);
        }
        if (dialog) {
            fn(dialog);
        } else {
            ons.createDialog('dialogs/loading.html')
            .then(function(dialog) {
                fn(dialog);
            });
        }
    },
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        ons.ready(function() {
            my.navigator = document.getElementById('my-navigator');
            my.navigator.onDeviceBackButton.disable(); // Disables back button handler
            // Onsen UI is now initialized
            ons.notification.alert({
                message : 'Ceci est un prototype d\'application.',
                title: 'ONLR',
                buttonLabel : "J'ai compris"
            });
            if(!localStorage.num || !localStorage.pwd)
                my.navigator.replacePage("pages/login.html");
            else
                my.connect();
            navigator.splashscreen.hide();
        });
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('init', function(event) {
            var btn;
            if(btn = event.target.querySelector('ons-toolbar div.right ons-toolbar-button[settings]')){
                btn.onclick = function(){
                    my.navigator.pushPage('pages/settings.html');                    
                }
            }
            if(event.target.matches("#register")){
                var btn = event.target.querySelector("ons-button[register]");
                btn.addEventListener("click", function(){
                    if(document.getElementById('password').value != document.getElementById('password2').value)
                        return ons.notification.alert({
                            message : 'Les deux PIN ne correspondent pas',
                            title: 'ONLR'
                        });
                    my.register(document.getElementById('username').value,document.getElementById('password').value);
                });
                btn = event.target.querySelector("ons-button[login]");
                btn.addEventListener("click", function(){
                    my.navigator.replacePage('pages/login.html');
                });
            }else if(event.target.matches("#login")){
                var btn = event.target.querySelector("ons-button[login]");
                btn.addEventListener("click", function(){
                    my.login(document.getElementById('username').value,document.getElementById('password').value);
                });
                btn = event.target.querySelector("ons-button[register]");
                btn.addEventListener("click", function(){
                    my.navigator.replacePage('pages/register.html');
                });
            };
        }, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        my.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        navigator.splashscreen.show();
    }
};
my.initialize();